#Javafx in a jar, generated with maven

This project contains the minimum required to build a jar containing a javafx application without the hassle of installing the SDK, modules, etc.

---

##Steps:
* launch maven: ```mvn clean package -U```
* launch the jar: ```java -jar [path]/example.jar``` (can be changed pom.xml:39)

